#!/usr/bin/env python3
import requests
from bs4 import BeautifulSoup as bs


class AppFinder:
    def __init__(self):
        self.rooms = 2
        self.webpage = "https://www.immowelt.de/liste/nuernberg/wohnungen/mieten?roomi={}&eqid=94&sort=relevanz".format(self.rooms)
        self.request = requests.get(self.webpage)
        self.soup = bs(self.request.text, 'html.parser')

    # DEBUG: print formatted HTML - for readability purposes
    #print(soup.prettify())

    def prices_list(self):
        prices = []
        prices_data_unformatted = self.soup.find_all("div", class_="price_rent")
        for price in prices_data_unformatted:
            prices.append(price.strong.get_text().replace("\r\n", "").strip())
        return prices

    def fetch_geolocation(self):
        locations = []
        raw_data = self.soup.find_all("span", class_="icon-map-marker")
        for loc in raw_data:
            locations.append(loc.next.strip())
        return locations

    def fetch_photo_links(self):
        photo_links = []
        for link in self.soup.find_all("img", class_="js_listimage"):
            photo_links.append(link.get('src'))
        return photo_links

