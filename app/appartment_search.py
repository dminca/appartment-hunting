
# coding: utf-8

# In[323]:


import requests as req
from bs4 import BeautifulSoup as bs
import pprint


# In[324]:


webpage = "https://www.immowelt.de/liste/nuernberg/wohnungen/mieten?roomi=2&eqid=94&sort=relevanz"
request = req.get(webpage)
soup = bs(request.text, 'html.parser')


# In[325]:


print(soup.prettify())


# In[349]:


prices_list = soup.find_all("div", class_="price_rent")
values = []
for i in prices_list:
    values.append(i.strong.get_text().replace("\r\n", "").strip())

# sort them
values.sort()


# In[350]:


pp = pprint.PrettyPrinter()
pp.pprint(values)

