FROM jupyter/scipy-notebook:17aba6048f44
RUN set -x \
  && pip freeze \
  && pip install \
    requests \
    scrapy \
    bs4
EXPOSE 8888
ENTRYPOINT ["start.sh", "jupyter", "lab", "--LabApp.token=''"]
