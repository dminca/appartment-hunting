# Appartment Hunting

Python 3.7 web scraping at its finest

## Jupyter Notebook

- [jupyter notebook launching in Docker][1]
- [jupyter notebook - selecting a Docker image][2]

## Resources

- [scraping for a rental home][3]
- [Python script for rental home scraper][4]
> this one includes plotting the values; also adds a map with Geolocation to point everything on a map
- [making a workflow with jupyter notebook][5]

[1]: https://jupyter-docker-stacks.readthedocs.io/en/latest/index.html
[2]: https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html
[3]: https://towardsdatascience.com/using-python-to-find-myself-a-rental-home-a0b1bf6f02e
[4]: https://github.com/reubenlee3/real-estate-rental/blob/master/script.py
[5]: https://technology.amis.nl/2019/02/06/running-an-enriched-jupyter-notebook-runtime-in-a-docker-container-locally-or-cloud-side/
